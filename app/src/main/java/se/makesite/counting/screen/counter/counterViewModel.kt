package se.makesite.counting.screen.counter

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class counterViewModel: ViewModel() {

    var counter = MutableLiveData<Int>();

    init {
        counter.postValue(0)
    }




    fun tick(){
        counter.postValue((counter.value ?: 0) + 1 )
    }



    fun reset(){

        counter.postValue(0)

    }

}