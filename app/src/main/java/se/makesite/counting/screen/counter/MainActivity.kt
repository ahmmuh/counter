package se.makesite.counting.screen.counter

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import se.makesite.counting.R
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = ViewModelProviders
            .of(this).get(counterViewModel::class.java)

        val binding: CounterActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel

        viewModel.counter.observe(this  , object : Observer<Int>{
            override fun onChanged(counter: Int?) {
                update(counter!!)

            }


        })

        reset_btn.setOnClickListener {
            viewModel.reset()
        }

        counter_btn.setOnClickListener {
            viewModel.tick()
        }
    }



    fun update(counter: Int){
        text_lable.text = counter.toString()

    }

}
